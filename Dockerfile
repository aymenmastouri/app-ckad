# Verwende ein Basis-Image mit OpenJDK 17 (oder der Java-Version, die du benötigst)
FROM openjdk:17-jdk-alpine

# Setze das Arbeitsverzeichnis im Container
WORKDIR /app

# Kopiere das JAR-File in den Container
COPY target/app-ckad-1.0.0-SNAPSHOT.jar /app/app-ckad.jar

# Exponiere den Port, auf dem die Anwendung läuft
EXPOSE 8080

# Starte die Anwendung
ENTRYPOINT ["java", "-jar", "/app/app-ckad.jar"]
